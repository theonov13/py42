from setuptools import setup

setup(name='py42kulk',
      version='0.1',
      description='The answer to everything!',
      url='https://gitlab.com/theonov13/py42/',
      author='Sebastian Schwalbe',
      author_email='thenov13@gmail.com',
      license='APACHE2.0',
      packages=['py42hulk'],
      zip_safe=False)
