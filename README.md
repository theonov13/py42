# Py42hulk

The answer to everything!

# Try to make a pip package. 
- [pypi](https://pypi.org/)
- [python-packaging](https://python-packaging.readthedocs.io/en/latest/minimal.html)

# Steps 
- make a repro at github/gitlab 
- structure 
    - [name]/[name]
    - [name]/setup.py
    - [name]/LICENSE
    - [name]/README.md 
    - make a user account at pypi 
    - $HOME/.pypirc 
    - install your package locally 
        - ```pip3 install -e .```
    - install your package at pypi/ test pypi 
        - `python3 setup.py sdist`
        - `python3 setup.py register sdist upload`
- `pip3 install [name]`

        
# setup.py 
```python   
from setuptools import setup  
  
setup(name='py42hulk',  
    version='0.1',   
    description='The answer to everything!',    
    url='https://gitlab.com/theonov13/py42/',   
    author='Sebastian Schwalbe',    
    author_email='thenov13@gmail.com',   
    license='APACHE2.0',   
    packages=['py42hulk'],   
    zip_safe=False)   
```

# .pypirc
```
[server-login]
username = theonov13
password = your_password
```       

# run py42hulk 
```python 
In [1]: from py42hulk import py42hulk
In [2]: p = py42hulk.py42hulk(a=1) 
In [3]: p.print() 
The answer is 42!
```

# Notes 
- check if the repro [name] is free
- in this test case py42 is not free 
- thus I renamed it to py42hulk 